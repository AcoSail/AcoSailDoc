import { groupCollapsed } from "console";

module.exports = {
  title: 'AcoSail V2 操作系统文档', // 标题
  description: 'Help you todo anything with AcoSail V2', // 副标题
  lang: 'zh-CN', //语言
  themeConfig: {
    // 顶部导航栏内容
    nav: [
      { text: '指南', link: '/guide/' },
      { text: '高阶指南', link: '/more/' },
    ],
    // 侧边栏导航内容
    sidebar: {
      '/guide/': [
        {
          text: '指南',
          collapsed: false,
          items: [
            {
              text: '简介',
              link: '/guide/',
            },
          ],
        },
        {
          text: '安装指南',
          collapsed: false,
          items: [
            {
              text: 'ASInstaller简介',
              link: '/guide/install/',
            },
            {
              text: '开发版安装',
              link: '/guide/install/install-dev',
            },
            {
              text: '开发版 with KDE安装',
              link: '/guide/install/install-KDE-dev',
            },
            {
              text: 'runtime版安装',
              link: '/guide/install/install-runtime',
            },
            {
              text: '常见安装问题',
              link: '/guide/install/FAQ',
            },
          ],
        },
        {
          text: '基本使用指南',
          collapsed: false,
          items: [
            {
              text: 'AcoSail_DWM桌面使用教程',
              link: '/guide/user/AcoSail_DWM',
            },
            {
              text: 'ST终端命令行使用教程',
              link: '/guide/user/st',
            },
            {
              text: 'AcoSail备份还原使用教程',
              link: '/guide/user/AcoSail_restore',
            },
            {
              text: 'acpidump工具使用教程',
              link: '/guide/user/AcoSail_acpidump',
            },
          ],
        },
        {
          text: '容器软件使用指南',
          collapsed: false,
          items: [
            {
              text: 'AcoSail V2 与 Docker',
              link: '/guide/docker-app/',
            },
            {
              text: 'Base Image',
              link: '/guide/docker-app/base',
            },
            {
              text: 'QT5 Image',
              link: '/guide/docker-app/QT5',
            },
          ],
        },
        {
          text: '过时内容',
          collapsed: true,
          items: [
            { text: '安装盘源', link: '/guide/V1/isorepo' },
            { text: '内网源', link: '/guide/V1/localrepo' },
          ]
        },
      ],
      '/more/': [
        {
          text: '高阶指南',
          items: [
            {
              text: '过时内容',
              collapsed: true,
              items: [
                { text: '简介', link: '/more/' },
                { text: 'uboot', link: '/more/uboot' },
                { text: 'nfs安装', link: '/more/nfsinstall' },
              ]
            },
          ],
        },
      ],
    },
  },
};