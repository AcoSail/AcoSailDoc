# AcoSail V1 ISO源配置

## 什么时候需要使用ISO源
1. 没有外网环境
2. 没有内网源
3. 需要安装Linux下常用软件包
  
## 使用ISO源需要的先决条件
1. 硬盘可用空间大于14GB
3. 安装ISO镜像文件
4. 拥有管理员账户权限（sudo/root权限）
5. 熟悉基础的Linux命令
6. 熟悉vi/vim命令

*选择最小安装会导致系统中没有wget等指令，流程会复杂一些*

### 挂载ISO源
首先将下载ISO镜像到/media下，x86_64下载acosail-8.6-x86_64-dvd.iso文件、aarch64下载acosail-8.6-aarch64-dvd.iso文件
之后再进行挂载操作
~~~bash
#x86_64机器执行此行
sudo wget https://repo.acosail.com/acosail/8/ISO/acosail-8.6-x86_64-dvd.iso /media/acosail-8.6-repo.iso
#aarch64机器执行此行
sudo wget https://repo.acosail.com/acosail/8/ISO/acosail-8.6-aarch64-dvd.iso /media/acosail-8.6-repo.iso

#机器本身无法连接https://repo.acosail.com/的时候请直接将通过其他方式获得的安装镜像cp、scp到/media/acosail-8.6-repo.iso
sudo mkdir -p /media/ISOrepo
sudo mount -o loop /media/acosail-8.6-repo.iso /media/ISOrepo
~~~

### 开机自动挂载ISO镜像
AcoSail与主流的发行版一致，可以使用/etc/fstab配置开机自动挂载选项，可以使用echo命令将配置写入文件末尾
~~~bash
sudo echo '/media/acosail-8.6-repo.iso /media/ISOrepo   iso9660 defaults,loop  0 0' >> /etc/fstab
~~~
*配置完成后需要重启生效*
## 修改AcoSail以使用ISO源
完成挂载操作后，还需要将我们的/etc/yum.repos.d/AcoSail-AppStream.repo和/etc/yum.repos.d/AcoSail-BaseOS.repo文件修改成指向ISO源的新配置
### 修改/etc/yum.repos.d/AcoSail-AppStream.repo
修改前：
~~~
# AcoSail-AppStream.repo
#

[appstream]
name=Aco Sail $releasever - AppStream
baseurl=https://repo.acosail.com/$contentdir/$releasever/AppStream/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-acosailofficial
~~~
修改后：
~~~
# AcoSail-AppStream.repo
#

[appstream]
name=Aco Sail $releasever - AppStream
baseurl=file:///media/ISOrepo/AppStream
gpgcheck=0
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-acosailofficial
~~~

### 修改/etc/yum.repos.d/AcoSail-BaseOS.repo
修改前：
~~~
# AcoSail-BaseOS.repo
#

[baseos]
name=Aco Sail $releasever - BaseOS
baseurl=https://repo.acosail.com/$contentdir/$releasever/BaseOS/$basearch/os/
gpgcheck=1
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-acosailofficial
~~~
修改后：
~~~
# AcoSail-BaseOS.repo
#

[baseos]
name=Aco Sail $releasever - BaseOS
baseurl=file:///media/ISOrepo/BaseOS
gpgcheck=0
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-acosailofficial
~~~
## 开始使用AcoSail源
完后以上的修改后，使用dnf或yum命令可以验证ISO源配置成功
~~~bash
sudo dnf update
~~~
~~~bash
#或使用yum命令
#sudo yum update
~~~
回显类似以下内容即可
~~~
Aco Sail 8 - AppStream                                                                  652 kB/s | 7.4 MB     00:11
Aco Sail 8 - BaseOS                                                                     226 kB/s | 2.4 MB     00:10
依赖关系解决。
无需任何处理。
完毕！
~~~