
# QT5 Image 镜像

## 简介

基于 Acosail runtime 版本制作的一个含有QT5运行环境的镜像。

## ChangeLog

``` shell
QT5 image (0.1)

    * Create QT5 image for Acosail

-- Acosail Dev Team  2023.01.10
```
