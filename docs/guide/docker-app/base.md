
# Base Image 镜像

## 简介

基于 Acosail runtime 版本制作的一个用于构建 docker 运行环境的镜像。

## ChangeLog

``` shell
Base image (0.1)

    * Create Base image for Acosail

-- Acosail Dev Team  2023.01.10
```
