
# 欢迎使用AcoSail

## 常见安装问题

1. Q : UEFI 模式下安装成功后，重启后如图所示，无法进入系统？
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_question_01.png"/>

   A : 这种情况是进入了 UEFI Shell，需要手动设置之后即可正常进入系统。

   ``` shell
    Shell> FS0:
    FS0:\> cd EFI\debian\
    FS0:\EFI\debian\> grubx64.efi
   ```

   > **说明**
   >
   > * 一般情况下是 FS0，当出现特殊情况时，需要根据提示来确认。
   >
   > * 不同架构下，执行的 grubXXX.efi 不一样，需要根据架构来区分。

2. Q : 如果等待时间超过1分钟也没有进入如下界面？
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_01.png"/>

   A ： 强制关机，并回到此界面，选择 **Advanced options -> Print mode**，同时拍摄视频或者保存串口信息并发送给翼辉 Acosail 团队，团队将会给出问题答复。

   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_00.png"/>

3. Q : 配置完安装选项之后，执行 **Install**，最后并没有显示 `Install finished`?
   
   A : 请提供如下页面的截图或者拍照给翼辉 Acosail 团队，团队将会给出问题答复。

   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_09.png"/>

4. Q : 安装之后重启超过10分钟都没有启动成功？
   
   A : 请提供相关的串口信息给翼辉 Acosail 团队，团队将会给出问题答复。
