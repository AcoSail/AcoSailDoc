# 欢迎使用 Acosail

## 自定义磁盘分区设置步骤

### 步骤 1: 进入自定义磁盘分区界面

在配置磁盘分区界面选择自定义磁盘分区，然后进入如下界面。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_custom_drive_01.png"/>

> **说明**
>
> * Create a new partition : 自定义划分每个分区的大小。
>
> * Suggest partition layout : 使用建议的分区计划（128M efi 分区，512M boot 分区，剩余的都划分给 根分区 ）。
>
> * Mark/Unmark full disk as lvm partition : 将全部磁盘标记/取消标记为 lvm 分区。

### 步骤 2: 自定义划分每个分区大小

1. 选择磁盘文件系统

    <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_custom_drive_02.png"/>

    > **说明**
    >
    > 建议选择 ext4 格式的文件系统

2. 设置分区的起始位置和大小。

    <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_custom_drive_03.png"/>

    > **说明**
    >
    > 首先添加的是 efi 分区，该分区起始位置为 **3MB**，大小为 **128MB**（如果觉得小，可以设置大一些）。

3. 设置新添加分区的挂载点。

    <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_custom_drive_04.png"/>

    选中之后，进入设置界面，在该界面中，选择刚刚新增的分区：

    <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_custom_drive_05.png"/>

    在该界面中，输入分区需要挂载的位置(此例中为 **/boot/efi**)：

    <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_custom_drive_06.png"/>

4. 继续新增分区。
   
   > **说明**
   >
   > * 需要至少三个分区（efi，boot，根分区）
   > * efi 分区大小一般在 500MB 左右。
   > * boot 分区大小一般在 512M ~ 1GB 左右。

### 步骤 3: 保存并退出分区设置
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_custom_drive_07.png"/>


