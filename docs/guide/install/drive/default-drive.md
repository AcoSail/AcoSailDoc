# 欢迎使用 Acosail

## 默认磁盘分区设置步骤

### 步骤 1: 进入默认磁盘分区界面

选择默认磁盘分支之后，直接进入到分区文件系统选择界面。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_default_drive_01.png"/>

> **说明**
>
> * 推荐使用 ext4 文件系统。

### 步骤 2: 选择是否单独划分home分区

当剩余磁盘大于32GB时，会提示是否将 /home 目录单独分区。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_default_drive_03.png"/>

如果选择 no，则将剩余的所有磁盘空间都划分给根分区使用；如果选择 yes，则将大小 20GB 的空间分配给根分区，剩余的分配给 home 分区。

### 步骤 3: 保存并退出磁盘分区界面

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_default_drive_05.png"/>

> **说明**
>
> **说明**
>
> * efi 分区默认大小 128MB。
> 
> * boot 分区默认大小 1GB。
>
> * root 分区默认大小 20GB。
>
> * home 分区占用剩余的所有分区大小。