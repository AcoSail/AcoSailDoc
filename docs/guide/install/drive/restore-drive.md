# 欢迎使用 Acosail

## 默认磁盘分区设置步骤

### 步骤 1: 进入默认磁盘分区界面

选择默认磁盘分支之后，直接进入到分区文件系统选择界面。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_default_drive_01.png"/>

> **说明**
>
> * 默认使用 ext4 文件系统。

### 步骤 2: 选择是否单独划分home分区

当剩余磁盘大于32GB时，会提示是否将 /home 目录单独分区。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_default_drive_03.png"/>

如果选择 no，则将剩余的所有磁盘空间都划分给备份还原使用；如果选择 yes，则进入到设置备份还原分区大小界面。默认大小 10GB。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_default_drive_04.png"/>

### 步骤 3: 完成分区

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_default_drive_02.png"/>

> **说明**
>
> * efi 分区默认大小 128MB。
> 
> * boot 分区默认大小 1GB。
>
> * root1、root2 分区默认大小 10GB。
> 
> * userdata 分区与 home 分区占有所有剩下的空间。