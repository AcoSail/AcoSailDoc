
# 欢迎使用AcoSail

## 启动盘制作

目前仅支持 **ventory** 或者 **etcher** 制作的启动盘，暂不支持包括**软碟通**在内的其他启动盘制作方式。

## 安装说明

目前 Acosail 提供两个版本（不同版本的安装步骤请参照对应版本），分别是：

- [基础版本](./install-dev.md)：基础内核的镜像版本。

- [开发版本](./install-dev.md)：预装了 Vscode 等开发套件的基础版本。

> 说明
>
> 1. 不管是基础版本还是开发版本，都可以在安装的时候选装 KDE 桌面环境。
>
> 2. 基础版本和开发版本，都可以在安装之后将基础内核升级到 RT 内核。
