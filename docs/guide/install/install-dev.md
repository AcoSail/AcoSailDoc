
# 欢迎使用AcoSail

## 安装教程

### 步骤 1: 使用 U 盘启动进入安装引导

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_00.png"/>

### 步骤 2: 选择 Install 选项进入安装菜单

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_01.png"/>

> **说明：**
>
> * 该菜单中标注了系统的登录用户名为 **acosail**，密码为' '。
> 
> * Locale language、Locale encoding、Hostname 这三项建议不要修改。
>
> * System restore 可以设置是否开启备份还原功能
> 
> * Drive(s) 可以设置系统的安装位置。
>
> * Base Image 可以设置系统的镜像版本。
>
> * 当最后的 Install 后面显示 config(s) missing 时，则说明有一项没有配置导致无法正常安装。

### 步骤 3: 设置备份还原功能

进入 System restore 菜单，默认是不开启备份还原，如果需要开启，则要求选择的安装磁盘必须大于26GB。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_10.png"/>

> **说明：**
>
> 开启备份还原功能之后，将只能使用默认分区方案，无法自定义分区。

### 步骤 4: 设置安装磁盘

进入 Drive(s) 菜单，选择系统安装磁盘，按 **TAB** 或**空格**选中，按**回车**保存并返回上级安装菜单。

<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_02.png"/>

### 步骤 5: 设置磁盘分区

1. 当设置完安装磁盘之后，顶级安装菜单会多出一项设置 **Disk layout**，这个设置是用来设置磁盘分区的。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_03.png"/>

2. 选择 **Disk layout** 进入磁盘分区界面后，没有特殊需求，可以选择推荐的磁盘分区方案，根据引导，一键设置。有特定需求的，可以选择自定义方案。当开启备份还原之后，只显示推荐的磁盘分区方案，自定义方案选项被隐藏。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_04.png"/>

   > **说明**
   >
   > 默认磁盘分区设置方式可参照[设置默认磁盘分区](./drive/default-drive.md)，自定义磁盘分区设置方式可参照[设置自定义磁盘分区](./drive/custom-drive.md)，开启备份还原磁盘分区设置方式可参照[设置备份还原状态下磁盘分区](./drive/restore-drive.md)。

3. 选择 **Base Image** 版本。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_05.png"/>

   > **说明**
   >
   > 默认是 runtime，提供两种版本： runtime 和 develop。runtime 是基础镜像，develop 则是在 runtime 的基础上增加了一些常见的开发套件。

    在 ARM64 环境中，如果选择 develop 版本时，会有额外的安装包菜单，提供 KDE 桌面环境(默认采用的是 DWM 桌面)。

    <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_06.png"/>

    <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_07.png"/>

4. 选择 **Install** 进行系统安装。

   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_08.png"/>

   当终端打印如下信息的时候，说明安装成功。

   <img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_install_09.png"/>

   此时可以按 **Enter** 重启进入 Acosail 系统，需要拔掉 U 盘。

   > **说明**
   >
   > 第一次启动会有较长时间的黑屏，此为正常现象！之后将保持正常水平（9-10s）!