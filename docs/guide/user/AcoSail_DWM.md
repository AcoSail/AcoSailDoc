
# AcoSail_DWM桌面使用教程

## 基本命令

| 常用快捷键 | 作用 |  
| :--- | :----: |  
| alt + shift + enter | 打开终端 |  
| alt + shift + c | 关闭当前终端 |  
| alt + d  | 窗口横向排列 |  
| alt + i | 窗口竖向排列 |  
| alt + enter | 窗口位置互换 |  
| alt + num | 切换标签页 |  
| alt + shift + num | 将当前窗口移动至某标签页 |  
| alt + m | 单窗口模式 |  
| alt + t | 窗口平铺模式 |  
| alt + p | 快速启动应用 |  

## 开机自动登录配置

### slim登陆管理器环境

Acosail 默认采用 slim 登录管理器，如果想设置自动登录的话，只需要修改配置文件 /etc/slim.conf 如下。

``` shell
# default user, leave blank or remove this line
# for avoid pre-loading the username.
default_user        acosail  #这边默认没开启，需要手动开启后，将用户名修改成自动登录的用户名

# Focus the password field on start when default_user is set
# Set to "yes" to enable this feature
#focus_password      no

# Automatically login the default user (without entering
# the password. Set to "yes" to enable this feature
auto_login          yes   #需要将这个选项开启并将no修改成yes
```

使用 default_user 字段结合 auto_login 字段实现自动登录功能。  

### lightdm 登录管理器环境

如果采用 lightdm 登录管理器，想要设置自动登录，则需要修改配置文件 /etc/lightdm/lightdm.conf 如下。

``` shell
#session-cleanup-script=
#autologin-guest=false
autologin-user=hkdshen     #这边默认是禁止状态，打开并将需要自动登录的用户名填写上
#autologin-user-timeout=0
#autologin-in-background=false
#autologin-session=
#exit-on-failure=false
```

修改完之后即可实现自动登录。