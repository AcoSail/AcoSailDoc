### 使用命令主要目的
硬件驱动加载不正确时, 可通过acpidump命令查看硬件信息

### 说明
ACPI 是一种开放标准，用于管理计算机硬件和操作系统之间的通信和交互，包括电源管理、设备配置、电源状态和系统休眠等功能。

`acpidump` 工具用于生成 ACPI（Advanced Configuration and Power Interface）表格数据。`acpidump` 工具能够读取系统中的 ACPI 表格，并将其以二进制形式转储到文件中。这些二进制文件可以进一步使用其他工具或解析器进行解析、分析和处理，以获取 ACPI 表格的具体内容和相关信息。

ACPI 表格包含了与系统硬件和配置相关的信息，如系统描述表（System Description Table，SDT）、固件描述表（Firmware Description Table，FDT）、不同设备的固件信息等。这些表格存储了有关硬件组件（例如处理器、内存、外围设备等）的详细信息和配置。

通过生成 ACPI 表格数据，开发人员、系统管理员或 ACPI 相关工具可以对系统的硬件配置和能力进行更深入的了解，并在需要时进行分析和调试。

### 安装
```bash
sudo apt install -y acpidump acpica-tools 
```
也可以手动下载安装
```bash
wget https://repo.acosail.com/acosail/V2_repo/pool/main/a/acpica-unix/acpica-tools_20200925-1.2_arm64.deb https://repo.acosail.com/acosail/V2_repo/pool/main/a/acpica-unix/acpica-tools_20200925-1.2_arm64.deb
apt install ./acpica-tools_20200925-1.2_arm64.deb ./acpica-tools_20200925-1.2_arm64.deb
```

### 选项说明
通用选项：
- `-b`：将acpi表格以二进制文件的形式进行转储, 需要工具解析
- `-h -?`：显示帮助信息
- `-o <File>`：将输出重定向到指定的文件
- `-r <Address>`：从指定的 RSDP（Root System Description Pointer）转储表格
	- RSDP地址可通过`dmesg | grep "ACPI: RSDP"`查看 
- `-s`：仅打印表格摘要
- `-v`：显示版本信息
- `-vd`：显示构建日期和时间
- `-z`：详细模式

表格选项：

- `-a <Address>`：通过物理地址获取表格
- `-c <on|off>`：打开/关闭自定义表格转储
- `-f <BinaryFile>`：通过二进制文件获取表格
- `-n <Signature>`：通过名称/签名获取表格
- `-x`：使用 RSDT（Root System Description Table）而不是 XSDT（Extended System Description Table）

不带参数调用该命令会转储所有可用的表格。 可以混合使用 `-a`、`-f` 和 `-n` 来获取多个表格实例。

### 基本功能示例

1. 使用实例: `sudo acpidump -b`
	- 当前目录生成二进制数据

	<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_acpidump_01.png"/>

|文件名|名称|内容说明|
|---|---|---|
|spcr.dat|Serial Port Console Redirection|该表格包含有关串口控制台重定向功能的信息|
|pptt.dat|Processor Properties Topology Table|该表格提供有关处理器拓扑结构和属性的信息|
|mcfg.dat|PCI Express Memory-Mapped Configuration Space Base Address Description Table|该表格描述了 PCI Express 设备的内存映射配置空间的基址|
|iort.dat|IO Remapping Table|该表格描述了输入/输出重映射的信息，用于管理 I/O 设备的地址和资源分配|
|gtdt.dat|Generic Timer Description Table|该表格提供了关于通用定时器的信息，用于处理处理器和系统中的计时器事件|
|facp.dat|Fixed ACPI Description Table|该表格包含了系统的固定 ACPI 描述信息，包括电源管理、电源按钮、休眠状态等|
|dsdt.dat|Differentiated System Description Table|该表格是一个固定的 ACPI 表格，包含了系统的不同硬件设备的定义和控制方法|
|dbg2.dat|Debug Port Table 2|该表格描述了系统的调试端口和调试相关的信息|
|bgrt.dat|Boot Graphics Resource Table|该表格包含了启动图形资源的信息，用于显示系统引导期间的图像|
|apic.dat|Advanced Programmable Interrupt Controller|该表格包含了关于高级可编程中断控制器的信息，用于管理和分发中断|


2. 查看数据
执行
```bash
iasl -d xxx.dat
cat xxx.dsl
```
- 解析数据

	<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_acpidump_02.png"/>

- 数据内容

	<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_acpidump_03.png"/>

- 各自段含义可以从对应版本的acpi文档手册中查看

	<img style="display: block; border: 1px solid #eeeeee" src="/images/acosail_acpidump_04.png"/>

acpi文档参考网址:
- https://uefi.org/specifications
- https://uefi.org/sites/default/files/resources/ACPI_Spec_6_4_Jan22.pdf