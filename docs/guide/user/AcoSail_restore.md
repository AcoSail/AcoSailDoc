# AcoSail 备份还原功能使用说明

## 概述

备份还原工具用于对系统文件和用户数据进行备份，或者在某次备份的基础上再次进行备份；支持将系统还原到某次备份时的状态。为用户提供了安全可靠的系统备份和恢复措施，降低了系统崩溃和数据丢失的风险。

### 备份还原机制

备份还原工具支持多种备份还原机制

| 模式 | 启动方式 | 适用场景 | 
| :--- | :----: |  :-----: |
|常规模式|开机启动系统，登录后运行备份还原工具|正常对系统进行备份还原|
|Grub 备份还原|在 grub 界面选择 Advanced options->Restore & Backup 运行备份还原工具|对系统进行备份，或者还原到任意一次成功备份的状态|

> **说明:**
>
> * 备份还原工具仅限系统管理员使用。
>
> * 备份还原的对象仅包括 /boot 分区， 根分区。

### 功能说明

#### 创建备份

1. 进入工具主界面。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_01.png"/>

2. 通过 Tab 键选中 Create Backup 按钮，然后回车进入创建备份界面。在输入框中输入相关的 comment 来标识这个备份。如果需要备份 boot 分区，则将复选框选中，然后选中 OK 按下回车开始创建备份。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_02.png"/>

   > **说明：**
   > * 输入框的q是占位符，不会显示到最终的comment中，属于显示问题，正在解决。
   >
   > * 目前备份 Boot 分区功能还在测试中，使用可能会存在问题。

3. 创建备份过程中可以通过 CANCLE 取消创建备份。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_03.png"/>

   当界面显示 Backup is complete 的时候，表示备份创建成功。此时按下回车将会回到主界面，并显示刚创建的备份条目。

   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_04.png"/>

#### 还原系统

##### 从本地还原系统

1. 在主界面中，选中需要还原的系统备份并按下 Restore 按钮。出现备份还原的提示。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_05.png"/>

2. 按下 Yes 进入系统还原前的准备工作，提示还原后系统将要重启。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_06.png"/>

3. 按下 OK 开始系统还原。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_07.png"/>

   > **说明:**
   >
   > * 还原过程中无法取消。

4. 还原成功，提示重启系统。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_08.png"/>

##### 从移动设备还原系统

1. 在主界面中，选中并按下 Restore From... 按钮，进入从移动设备还原系统界面。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_14.png"/>

2. 先按下 Mount 按钮，进入移动设备挂载界面。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_10.png"/>

   在该界面中先通过上下键选择可移动设备，然后按空格键选中。最后通过 Tab 键选中 OK 按钮，回车键确定后，挂载可移动设备。提示挂载成功后，选中并按下 CANCLE 按钮返回从移动设备还原系统界面。

3. 从移动设备中选中需要还原的备份条目，然后选中并按下右侧的 Restore 按钮，出现备份还原提示。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_15.png"/>

4. 按下 Yes 进入系统还原前的准备工作，提示还原后系统将要重启。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_06.png"/>

5. 按下 OK 开始系统还原。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_07.png"/>

6. 还原成功，提示重启系统。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_08.png"/> 

#### 管理备份

##### 导入导出备份

1. 在主界面中按下 Manage 按钮进入管理界面。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_09.png"/>

2. 先按下 Mount 按钮，进入移动设备挂载界面。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_10.png"/>

   在该界面中先通过上下键选择可移动设备，然后按空格键选中。最后通过 Tab 键选中 OK 按钮，回车键确定后，挂载可移动设备。提示挂载成功后，选中并按下 CANCLE 按钮返回管理界面。

3. 管理界面右侧显示移动设备中的目录信息。先通过 Tab 键将焦点切换到右侧区域之后，选择导出备份的存放目录。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_11.png"/>

4. 在左侧区域选择需要导出的备份条目，然后选中并按下 Move Out->，将备份文件导出到移动设备中。右侧区域出现备份文件，备份成功。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_12.png"/>

##### 删除备份

1. 在主界面中按下 Manage 按钮进入管理界面。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_09.png"/>

2. 在左侧区域选中需要删除的备份条目，然后 Tab 选中 Remove 按钮，按下 Remove 按钮，提示是否删除选中条目。确认后删除。
   
   <img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_13.png"/>


## 操作步骤

### 常规模式

#### 步骤 1: 进入系统，在终端运行命令启动备份还原工具。

``` shell
acosail@acosail:~$ sudo restory
```

#### 步骤 2: 进行相关功能操作。

### Grub 模式

#### 步骤 1: 进入 Grub 界面。

<img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_16.png"/>

#### 步骤 2: 进入 Advanced options for Acosail GNU/Linux 选项。

<img style="display: block; border: 1px solid #eeeeee" src="/images/restore/acosail_restore_17.png"/>

#### 步骤 3: 选择 Acosail GNU/Linux. with Linux 4.19.115-bpo.4 (Restore & Backup), 稍后进入备份还原主界面。

#### 步骤 4: 进行相关功能操作。