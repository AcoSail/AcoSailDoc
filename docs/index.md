---
layout: home

title: AcoSail V2
titleTemplate: 操作系统文档

hero:
  name: AcoSail V2
  text: 操作系统文档
  tagline: 在这里可以获取所有有关AcoSail操作系统问题的信息，以及及时更新的更多有价值的文档
  image:
    src: /sail_onebot.webp
    alt: AcoSail
  actions:
    - theme: brand
      text: 获取指南
      link: /guide/
    - theme: alt
      text: 更多信息
      link: /more/

features:
  - icon: 💡
    title: 兼容性原则
    details: 兼容主流Linux环境，容器化技术，当前内核版本为5.15 LTS版本；如果您不满意，可以自行升级到更新的版本，当然我们也相信您有能力处理带来的新问题。
  - icon: ⚡️
    title: 原生支持RT内核
    details: 我们针对特定实时性用户提供特殊的便捷安装工具，方便您随时体验我们RT内核版本。
  - icon: 🛠️
    title: 丰富的工具链
    details: 除了原生提供标准的GNU的编译工具意外，我们也有LLVM、java、nodejs、python、php等主流开发工具链。
  - icon: 📦
    title: 翼辉云加持
    details: AcoSail真正的力量源于翼辉云的加持，AcoSail在翼辉云开放平台里是第一个验证并使用的平台，一定会使您的开发事半功倍。
---
