# AcoSail相关的高阶使用手册

## AcoSail V1和uboot
在特定的嵌入式环境中尝试使用AcoSail V1嵌入式版，由于平台限制需要使用uboot引导系统，可以参照本教程的uboot章节和nfs安装章节进行。

::: danger
AcoSail V1默认并不支持uboot，如果需要使用支持uboot版本的AcoSail V1，您应该首先了解uboot、linux内核以及其他的基础知识。AcoSail V1如同一般的Linux发行版一样开放对应的内核源码，但并不保证在任何机器上都能完美的运行。如果您遇到问题，可以寻求社区的帮助。
:::

TO be added...