# 通过使用NFS安装AcoSail V1（uboot环境）

在没有安装系统的机器上，进行以下操作即可在uboot下使用NFS的方式安装AcoSail

## 配置网络
~~~sh
setenv ipadsetenv ipaddr 10.10.20.48
setenv serverip 10.10.20.149
setenv netmask 255.255.255.0
setenv gatewayip 10.10.20.201
setenv eth1addr 72:e9:25:cd:e3:36r 10.10.20.48
setenv serverip 10.10.20.149
setenv netmask 255.255.255.0
setenv gatewayip 10.10.20.201
setenv eth1addr 72:e9:25:cd:e3:36
~~~

## 网络加载文件
~~~sh
tftpboot 0x90100000 ft2004-devboard-d4-dsk.dtb
tftpboot 0x90200000 uImage_acosail
tftpboot 0x93000000 initramfs-uboot.img

setenv bootargs console=ttyAMA1,115200 earlycon=pl011,0x28001000 root=nfs4:10.10.20.149:/installer/rootfs rw ip=10.10.20.43 bootdev=eth0 initrd=0x93000000,120M
~~~

## 执行启动
~~~sh
bootm 0x90200000 - 0x90100000
~~~

## nfs安装
在网络良好的情况下，执行启动完成后将进入到类似UEFI下安装嵌入式AcoSail的界面，建议等待15S后选择您想安装的磁盘，并执行install操作即可完成安装。
完成安装后关机并在己进入到uboot环境后，您将可以配置从硬盘启动


## 配置从nvme硬盘启动
~~~sh
setenv load_fdt "ext4load nvme 0:2 0x90100000 ft2004-devboard-d4-dsk.dtb"
setenv load_initrd "ext4load nvme 0:2 0x93000000 initramfs-uboot.img"
setenv load_kernel "ext4load nvme 0:2 0x90200000 uImage_acosail"
setenv distro_bootcmd "run load_fdt;run load_kernel;run load_initrd;run boot_fdt"
setenv board_name ft2004
setenv boot_fdt "bootm 0x90200000 - 0x90100000"

env save
~~~

::: danger
由于安装时nfs对于网络造成的压力极大，使用前请再三确认本安装设备和nfs服务器与一般工作、机房网络的物理隔离状态！！！
:::