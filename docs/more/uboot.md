# AcoSail V1 UBOOT 使用方法 v0.1

在已经安装好原裁剪版的基础上，进行以下操作即可使用UBOOT方式启动镜像

## 配置网络
~~~sh
setenv ipadsetenv ipaddr 10.10.20.48
setenv serverip 10.10.20.149
setenv netmask 255.255.255.0
setenv gatewayip 10.10.20.201
setenv eth1addr 72:e9:25:cd:e3:36r 10.10.20.48
setenv serverip 10.10.20.149
setenv netmask 255.255.255.0
setenv gatewayip 10.10.20.201
setenv eth1addr 72:e9:25:cd:e3:36
~~~

## 网络加载文件
~~~sh
tftpboot 0x90100000 ft2004-devboard-d4-dsk.dtb
tftpboot 0x90200000 uImage_acosail
tftpboot 0x93000000 initramfs-uboot.img

setenv bootargs console=ttyAMA1,115200 earlycon=pl011,0x28001000 root=/dev/nvme0n1p3  rootfstype=ext4 rootdelay=5 rw initrd=0x93000000,120M
~~~

## 执行启动
~~~sh
bootm 0x90200000 - 0x90100000
~~~

## 成功启动后您可能还想继续进行的操作
- 将uImage_acosail、initramfs-uboot.img和ft2004-devboard-d4-dsk.dtb文件拷贝到/boot文件夹下
- 将配套的modules放置在/lib/modules下
- 执行depmod
- 安装您想安装的rpm包
- 配置系统网络

## 配置从nvme硬盘启动
~~~sh
setenv load_fdt "ext4load nvme 0:2 0x90100000 ft2004-devboard-d4-dsk.dtb"
setenv load_initrd "ext4load nvme 0:2 0x93000000 initramfs-uboot.img"
setenv load_kernel "ext4load nvme 0:2 0x90200000 uImage_acosail"
setenv distro_bootcmd "run load_fdt;run load_kernel;run load_initrd;run boot_fdt"
setenv board_name ft2004
setenv boot_fdt "bootm 0x90200000 - 0x90100000"

env save
~~~

## 关于启动地址
不同的设备uboot的引导地址不同。下面罗列我们已支持的设备信息：

| 设备 | 启动地址 | entry point |  
|  ----  | ----  |  ----  | 
| D2000 | 0x90200000 | - |  
| FT2004| 0x90200000 | - |  
| FT2000AHK | 0x90200000 |  0x90200000 |  
